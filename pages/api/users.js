import colors from '../../shared/colors';

const fetch = require('cross-fetch');

const users = async (req, res) => {
  try {
    const users = await fetch(`https://testapi.io/api/deborae/users`);
    const usersData = await users.json();

    const posts = await fetch(`https://testapi.io/api/deborae/posts`);
    const postsData = await posts.json();

    const usersDataWithPostCount = usersData
      .map((user, i) => {
        let postCount = 0;

        for (const post of postsData) {
          if (post.userId === user.id) {
            postCount += 1;
          }
        }

        return {
          ...user,
          postCount,
          color: colors[i],
        };
      })
      .filter(user => user.postCount >= 1);

    res.status(200).json(usersDataWithPostCount);
  } catch (error) {
    throw new Error(error);
  }
};

export default users;
