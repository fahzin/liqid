/* eslint-disable id-length */
import Head from 'next/head';
import {useEffect, useState, useRef} from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import '../assets/css/global.css';
import 'sanitize.css';
import * as S from '../assets/css/shared.styled';

const margin = {top: 30, right: 20, bottom: 30, left: 50};
const width = 800 - margin.left - margin.right;
const height = 600 - margin.top - margin.bottom;

const Index = ({data}) => {
  const [documentReady, setDocumentReady] = useState(false);
  const d3svg = useRef(null);

  const chart = () => {
    const svg = d3.select(d3svg.current);

    const x = d3
      .scaleBand()
      .domain(data.map(d => d.username))
      .range([margin.left, width - margin.right])
      .padding(0.1);

    const y = d3
      .scaleLinear()
      .domain([0, d3.max(data, d => d.postCount)])
      .range([height - margin.bottom, margin.top]);

    const yAxis = g =>
      g.attr('transform', `translate(${margin.left},0)`).call(d3.axisLeft(y));

    const xAxis = g =>
      g
        .attr('transform', `translate(0,${height - margin.bottom})`)
        .call(d3.axisBottom(x));

    const div = d3
      .select('body')
      .append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0);

    svg
      .selectAll('.bar')
      .data(data)
      .enter()
      .append('rect')
      .style('fill', d => d.color)
      .style('cursor', 'pointer')
      .attr('class', 'bar')
      .attr('x', d => x(d.username))
      .attr('y', d => y(d.postCount))
      .attr('width', x.bandwidth())
      .attr('height', d => y(0) - y(d.postCount))
      .on('mouseover', d => {
        div.style('opacity', 1);
        div
          .html(
            `<strong>${
              d.username
            }</strong> <span class='color-tag' style='background-color: ${
              d.color
            }'></span> <br /><strong>Posts:</strong> ${
              d.postCount
            } <br /> <strong>Name:</strong> ${
              d.name
            } <br /> <strong>E-mail:</strong> ${d.email}`
          )
          .style('left', `${d3.event.pageX}px`)
          .style('top', `${d3.event.pageY - 28}px`);
      })
      .on('mouseout', () => {
        div.style('opacity', 0);
      });

    svg.append('g').call(xAxis);
    svg.append('g').call(yAxis);
  };

  useEffect(() => {
    if (typeof window !== 'undefined') {
      setDocumentReady(true);
    }
  }, []);

  useEffect(() => {
    if (documentReady) {
      chart();
    }
  }, [documentReady]);

  return (
    <>
      <Head>
        <title>LIQID Test</title>
      </Head>
      <S.MainWrapper>
        {!documentReady ? (
          'Loading...'
        ) : (
          <S.ChartWrapper>
            <svg
              viewBox={`0 0 ${width} ${height}`}
              className='bar-chart-container'
              role='img'
              ref={d3svg}
            />
          </S.ChartWrapper>
        )}
      </S.MainWrapper>
    </>
  );
};

Index.getInitialProps = async () => {
  const res = await fetch('http://localhost:3000/api/users');
  const data = await res.json();
  return {data};
};

Index.propTypes = {
  data: PropTypes.array.isRequired,
};

export default Index;
