'use strict';

module.exports = {
  env: {
    browser: true,
    es6: true,
    commonjs: true,
  },
  extends: [
    '@strv/javascript/environments/react/v16',
    '@strv/javascript/environments/react/optional',
    '@strv/javascript/environments/react/accessibility',
    '@strv/javascript/coding-styles/recommended',
    'plugin:prettier/recommended',
    'prettier',
    'prettier/react',
  ],
  parser: 'babel-eslint',
  parserOptions: {
    allowImportExportEverywhere: true,
  },
  globals: {
    document: true,
    window: true,
    process: true,
  },
  rules: {
    'prettier/prettier': 'error',
    'import/no-extraneous-dependencies': 0,
    'import/extensions': 0,
    'react/jsx-filename-extension': 0,
    'import/no-self-import': false,
    'import/no-cycle': false,
    'import/no-useless-path-segments': false,
    'import/group-exports': false,
    'import/no-unresolved': 0,
    'linebreak-style': 0,
    'react/react-in-jsx-scope': 'off',
    'jsx-a11y/anchor-is-valid': 'ignore',
    'no-shadow': 'off',
    'no-warning-comments': [
      0,
      {terms: ['todo', 'fixme', 'any other term'], location: 'anywhere'},
    ],
  },
};
