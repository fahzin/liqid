# LIQID

LIQID Code Challenge

## Install & Usage

1. Install project dependencies, run the command below:

```bash
yarn
```

2. start the development server:

```bash
yarn dev
```

## Build for production

1. generate a production build:

```bash
yarn build
```

2. start a production server:

```bash
yarn start
```